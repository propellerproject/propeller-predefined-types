# Propeller Predefined Types #

This repository contains [JSON] files for all of [Propeller]’s predefined types.

  [JSON]: https://www.json.org

  [Propeller]: https://propeller-project.github.io
