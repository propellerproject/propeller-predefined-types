.POSIX:
.SUFFIXES:
.SUFFIXES: .json .json-f

SHELL                           = /bin/sh

DIFF                            = diff
JQ                              = jq
JQ_FLAGS                        = --indent 2 --ascii-output --sort-keys
MV                              = mv
RM                              = rm -f
TAR                             = tar
TAR_FLAGS                       = --no-acls --no-selinux --no-xattrs --posix --auto-compress --verbose

jsons                           = \
data/atom-package.json            \
data/autotools.json               \
data/cmake.json                   \
data/go.json                      \
data/gradle.json                  \
data/grunt.json                   \
data/jekyll.json                  \
data/make.json                    \
data/maven.json                   \
data/module-build.json            \
data/msbuild.json                 \
data/node-package.json            \
data/premake4.json                \
data/waf.json

formatted                       = $(jsons:.json=.json-f)

package                         = propeller-predefined-types.tar.gz

all: check

check: $(formatted)
	for json in $(jsons) ; \
	do \
		$(DIFF) $${json} $${json}-f ; \
	done

clean: distclean
	$(RM) $(formatted)

dist: $(package)

distclean:

format: $(formatted)
	for json in $(jsons) ; \
	do \
		$(MV) $${json}-f $${json} ; \
	done

print-%:
	$(info $*=$($*))
	@:

.PHONY: all check clean dist distclean print-%

$(package):
	$(RM) $@
	$(TAR) $(TAR_FLAGS) -c -f $@ $(jsons)

.json.json-f:
	$(RM) $@
	$(JQ) $(JQ_FLAGS) . < $< > $@
